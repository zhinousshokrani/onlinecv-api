using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.Controllers
{
    [Route("api/[controller]")]
    public class EducationController : Controller
    {
        private IEducationService _educationService;
        public EducationController(IEducationService educationService)
        {
            _educationService = educationService;
        }

        // Get api/education/cvId
        [HttpGet("{cvId}")]
        public IActionResult Get(int cvId)
        {
            try
            {
                return Ok(_educationService.GetByCVId(cvId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }

        // POST api/education
        [HttpPost]
        public IActionResult Post([FromBody]EducationHistory educationHistory)
        {
            try
            {
                _educationService.SaveEducationHistory(educationHistory);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }

    }
}
