using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.Controllers
{
    [Route("api/[controller]")]
    public class SkillController : Controller
    {
        private ISkillService _skillService;
        public SkillController(ISkillService skillService)
        {
            _skillService = skillService;
        }

        // Get api/skill/cvId
        [HttpGet("{cvId}")]
        public IActionResult Get(int cvId)
        {
            try
            {
                return Ok(_skillService.GetByCVId(cvId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }

        // POST api/skill
        [HttpPost]
        public IActionResult Post([FromBody]SkillHistory skillHistory)
        {
            try
            {
                _skillService.SaveSkillHistory(skillHistory);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }

    }
}
