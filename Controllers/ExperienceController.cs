using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.Controllers
{
    [Route("api/[controller]")]
    public class ExperienceController : Controller
    {
        private IJobExperienceService _experienceService;
        public ExperienceController(IJobExperienceService experienceService)
        {
            _experienceService = experienceService;
        }

        // Get api/experience/cvId
        [HttpGet("{cvId}")]
        public IActionResult Get(int cvId)
        {
            try
            {
                return Ok(_experienceService.GetByCVId(cvId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }

        // POST api/experience
        [HttpPost]
        public IActionResult Post([FromBody]WorkHistory experienceHistory)
        {
            try
            {
                _experienceService.SaveWorkHistory(experienceHistory);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }

    }
}
