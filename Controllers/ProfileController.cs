using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProfileController : Controller
    {
        private IPersonalProfileService _profileService;
        private ICVService _cvService;
        public ProfileController(IPersonalProfileService profileService,ICVService cvService)
        {
            _profileService = profileService;
            _cvService = cvService;
        }

        // POST api/profile
        [HttpPost]
        public IActionResult Post([FromBody]ProfileAndTemplate obj)
        {
            try
            {
                int profileId = _profileService.SavePersonalPrfile(obj.profile);
                int cvId=_cvService.SaveCV(profileId,obj.templateId);
                
                if (profileId == -1 || cvId == -1)
                    return StatusCode(500, "An error ocuured in creating profile profile");

                return Ok(cvId);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }

    }
}
