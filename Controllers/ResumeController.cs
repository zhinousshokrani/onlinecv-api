using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;

namespace OnlineCVAPI.Controllers
{
    [Route("api/[controller]")]
    public class ResumeController : Controller
    {
        private IResumeService _resumeService;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ResumeController(IResumeService resumeService, IPersonalProfileService profileService,
        IHostingEnvironment hostingEnvironment)
        {
            _resumeService = resumeService;
            _hostingEnvironment = hostingEnvironment;
        }
        // Get api/resume/cvId
        [HttpGet("{cvId}")]
        public IActionResult Get(int cvId)
        {
            try
            {
                 var stream = _resumeService.GetResume(cvId);
                if (stream != null)
                    return File(stream, "application/octet-stream");
                else
                   return StatusCode(500, "Error in downloading CV");
            }
            catch (Exception ex)
            {
                   return StatusCode(500, ex.ToString());
            }
        }
    }
}
