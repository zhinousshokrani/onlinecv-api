using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.Helpers
{
    public class WordProcessor : IWordProcessor
    {
        public FileStream ReplaceWordTemplate(
            string contentRootPath
            , string templateFileName
            , PersonalProfile profile
            , List<Education> educationHitstory
            , List<Experience> workHistory
            , List<Skill> skills)
        {
            var documentPath = string.Format("{0}{1}{2}", contentRootPath, @"\Templates\", templateFileName);

            if (File.Exists(documentPath))
            {
                var tempPath = string.Format("{0}{1}{2}", contentRootPath, @"\Templates\", profile.email + ".docx");

                File.Copy(documentPath, tempPath, true);

                using (WordprocessingDocument doc = WordprocessingDocument.Open(tempPath, true))
                {
                    ReplaceProfileInBody(doc.MainDocumentPart.Document.Body, profile);
                    ReplaceProfileInFooter(doc.MainDocumentPart.FooterParts, profile);
                    ReplaceProfileInHeader(doc.MainDocumentPart.HeaderParts, profile);
                    ReplaceResumeContent(doc.MainDocumentPart.Document.Body
                    , educationHitstory,workHistory,skills);
                    doc.MainDocumentPart.Document.Save();
                }
                return new FileStream(tempPath, FileMode.Open);
            }
            return null;
        }

        private void ReplaceProfileInBody(Body body, PersonalProfile profile)
        {
            foreach (var text in body.Descendants<Text>())
            {
                ReplaceTextElement(text, profile);
            }
        }

        private void ReplaceProfileInFooter(IEnumerable<FooterPart> footerParts, PersonalProfile profile)
        {
            foreach (var footer in footerParts)
            {
                foreach (var text in footer.RootElement.Descendants<Text>())
                {
                    ReplaceTextElement(text, profile);
                }
            }
        }

        private void ReplaceProfileInHeader(IEnumerable<HeaderPart> headerParts, PersonalProfile profile)
        {
            foreach (var header in headerParts)
            {
                foreach (var text in header.RootElement.Descendants<Text>())
                {
                    ReplaceTextElement(text, profile);
                }
            }
        }

        private void ReplaceTextElement(Text text, PersonalProfile profile)
        {
            if (text.Text.Contains("[FIRSTNAME]"))
            {
                text.Text = text.Text.Replace("[FIRSTNAME]", profile.firstName);
            }
            else if (text.Text.Contains("[LASTNAME]"))
            {
                text.Text = text.Text.Replace("[LASTNAME]", profile.lastName);
            }
            else if (text.Text.Contains("[ADDRESS]"))
            {
                text.Text = text.Text.Replace("[ADDRESS]", profile.address);
            }
            else if (text.Text.Contains("[PHONE]"))
            {
                text.Text = text.Text.Replace("[PHONE]", profile.phoneNumber);
            }
            else if (text.Text.Contains("[EMAIL]"))
            {
                text.Text = text.Text.Replace("[EMAIL]", profile.email);
            }
        }

        private void ReplaceResumeContent(Body body
            , List<Education> educationList
            , List<Experience> jobList
            , List<Skill> skillList)
        {
            var bookmarkStarts = body.Descendants<BookmarkStart>();
            var bookmarkEnds = body.Descendants<BookmarkEnd>();

            IDictionary<String, BookmarkStart> bookMarkMap = new Dictionary<String, BookmarkStart>();
            foreach (BookmarkStart bookMarkStart in bookmarkStarts)
            {
                bookMarkMap[bookMarkStart.Name] = bookMarkStart;
            }

            foreach (BookmarkStart bookMarkStart in bookMarkMap.Values)
            {
                var parent = bookMarkStart.Parent;

                if (bookMarkStart.Name == "EducationList")
                {
                    ReplaceEducationHistory(parent, educationList);
                }
                else if (bookMarkStart.Name == "JobList")
                {
                    ReplaceWorkHistory(parent, jobList);
                }
                else if (bookMarkStart.Name == "SkillList")
                {
                    ReplaceSkillHistory(parent, skillList);
                }
            }

            //Remove All Bookmarks after replace
            foreach (BookmarkStart bookMarkStart in bookmarkStarts)
            {
                bookMarkStart.RemoveAllChildren();
                bookMarkStart.Remove();
            }
            foreach (BookmarkEnd bookMarkEnd in bookmarkEnds)
            {
                bookMarkEnd.RemoveAllChildren();
                bookMarkEnd.Remove();
            }
        }

        private void ReplaceEducationHistory(OpenXmlElement parent, List<Education> educationList)
        {
            Paragraph paragraph;
            Text text;
            Run run;
            RunProperties runProperties;

            for (int i = 0; i < educationList.Count; i++)
            {
                paragraph = new Paragraph();

                //Title                
                run = new Run();
                runProperties = new RunProperties();
                runProperties.Bold = new Bold();
                run.Append(runProperties);
                text = new Text(educationList[i].title) { Space = SpaceProcessingModeValues.Preserve };
                run.Append(text);
                run.AppendChild(new Break());
                paragraph.Append(run);

                //Institute
                text = new Text(educationList[i].institute) { Space = SpaceProcessingModeValues.Preserve };
                run = new Run();
                run.Append(text);
                run.AppendChild(new Break());
                paragraph.Append(run);

                //Graduation Year                
                text = new Text("Graduated " + educationList[i].graduationYear) { Space = SpaceProcessingModeValues.Preserve };
                run = new Run();
                run.Append(text);
                if (i != educationList.Count - 1) run.AppendChild(new Break());
                paragraph.Append(run);

                parent.InsertBeforeSelf<Paragraph>(paragraph);
            }
        }

        private void ReplaceWorkHistory(OpenXmlElement parent, List<Experience> jobList)
        {
            Paragraph paragraph;
            Text text;
            Run run;
            RunProperties runProperties;

            for (int i = 0; i < jobList.Count; i++)
            {
                paragraph = new Paragraph();

                //Title                
                run = new Run();
                runProperties = new RunProperties();
                runProperties.Bold = new Bold();
                run.Append(runProperties);
                text = new Text(jobList[i].jobTitle) { Space = SpaceProcessingModeValues.Preserve };
                run.Append(text);
                run.AppendChild(new Break());
                paragraph.Append(run);

                //set job duration time
                var startDate = jobList[i].startMonth + " " + jobList[i].startYear;
                var endDate = jobList[i].endMonth + " " + jobList[i].endYear;
                if (jobList[i].stillInRole)
                    endDate = "Current";
                //Employer
                text = new Text(jobList[i].Employer + ", " + startDate + " - " + endDate) { Space = SpaceProcessingModeValues.Preserve };
                run = new Run();
                run.Append(text);
                if (i != jobList.Count - 1) run.AppendChild(new Break());
                paragraph.Append(run);

                parent.InsertBeforeSelf<Paragraph>(paragraph);
            }
        }

        private void ReplaceSkillHistory(OpenXmlElement parent, List<Skill> skillList)
        {
            Paragraph paragraph;
            Text text;
            Run run;
            RunProperties runProperties;

            for (int i = 0; i < skillList.Count; i++)
            {
                paragraph = new Paragraph();

                //Title                
                run = new Run();
                runProperties = new RunProperties();
                runProperties.Bold = new Bold();
                run.Append(runProperties);
                text = new Text(skillList[i].skillName +" , "+ skillList[i].Institute + " - " + skillList[i].year) { Space = SpaceProcessingModeValues.Preserve };
                run.Append(text);
                if (i != skillList.Count - 1) run.AppendChild(new Break());
                paragraph.Append(run);

                parent.InsertBeforeSelf<Paragraph>(paragraph);
            }
        }
    }
}