using System.Collections.Generic;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.ViewModels
{
    public class WorkHistory
    {
        public List<Experience> jobList {get; set;}
        public int cvId {get; set;}
    }
}