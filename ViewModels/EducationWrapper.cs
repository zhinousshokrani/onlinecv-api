namespace OnlineCVAPI.ViewModels
{
    public class EducationWrapper
    {
        public string title {get; set;}
        public string institute {get; set;}
        public string graduationYear {get; set;}
    }
}