using System.Collections.Generic;
using System.Linq;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.DataAccessLayer;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;
using System.IO;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.AspNetCore.Hosting;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using OnlineCVAPI.Helpers;

namespace OnlineCVAPI.BusinessLayer
{
    public class ResumeService : IResumeService
    {
        IGenericRepository<CV> _cvRepository;
        IGenericRepository<CVTemplate> _templateRepository;
        IGenericRepository<PersonalProfile> _profileRepository;
        IGenericRepository<Education> _educationRepository;
        IGenericRepository<Experience> _jobRepository;
        IGenericRepository<Skill> _skillRepository;
        IWordProcessor _wordProcessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ResumeService(
                IGenericRepository<CV> cvRepository,
                IGenericRepository<CVTemplate> templateRepository,
                IGenericRepository<PersonalProfile> profileRepository,
                IGenericRepository<Education> educationRepository,
                IGenericRepository<Experience> jobRepository,
                IGenericRepository<Skill> skillRepository,
                IWordProcessor wordProcessor,
                IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _cvRepository = cvRepository;
            _templateRepository = templateRepository;
            _profileRepository = profileRepository;
            _educationRepository = educationRepository;
            _jobRepository = jobRepository;
            _skillRepository = skillRepository;
            _wordProcessor = wordProcessor;
        }

        public CV GetById(int cvId)
        {
            return _cvRepository.GetById(cvId);
        }

        public FileStream GetResume(int cvId)
        {
            var cv = GetById(cvId);
            if (cv != null)
            {
                
                var profile = _profileRepository.GetById(cv.profileId);
                var templateId = cv.templateId;
                if (profile != null && templateId > 0)
                {
                    var templateFileName = _templateRepository.GetById(templateId)?.templateDocFile;
                    if (!String.IsNullOrEmpty(templateFileName))
                    {
                        return _wordProcessor.ReplaceWordTemplate(
                            _hostingEnvironment.ContentRootPath,
                            templateFileName, 
                            profile,
                            _educationRepository.GetAll().Where(x=>x.CVId==cvId).ToList(),
                            _jobRepository.GetAll().Where(x=>x.CVId==cvId).ToList(),
                            _skillRepository.GetAll().Where(x=>x.CVId==cvId).ToList()
                            );
                    }
                }
            }
            return null;
        }

    }

}