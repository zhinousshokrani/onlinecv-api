using System.Collections.Generic;
using System.Linq;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.DataAccessLayer;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.BusinessLayer
{
    public class JobExperienceService : IJobExperienceService
    {
        IGenericRepository<Experience> _repository;
        public JobExperienceService(IGenericRepository<Experience> repository)
        {
            _repository = repository;
        }

        public List<ExperienceWrapper> GetByCVId(int cvId)
        {
            return (from e in _repository.GetAll()
                    where e.CVId == cvId
                    select new ExperienceWrapper
                    {
                        jobTitle = e.jobTitle,
                        Employer=e.Employer,
                        Location=e.Location,
                        startMonth=e.startMonth,
                        startYear=e.startYear,
                        endMonth=e.endMonth,
                        endYear=e.endYear,
                        stillInRole=e.stillInRole
                    }).ToList();
        }

        public void SaveWorkHistory(WorkHistory workHistory)
        {
            foreach (Experience exp in workHistory.jobList)
                exp.CVId = workHistory.cvId;

            //check if this cv id has previously work history
            var previousJobList = _repository.GetAll().Where(x => x.CVId == workHistory.cvId).ToArray();
            int count = _repository.GetAll().Where(x => x.CVId == workHistory.cvId).Count();
            if (count > 0)
                _repository.DeleteRange(previousJobList);

            _repository.CreateRange(workHistory.jobList);
        }
    }
}