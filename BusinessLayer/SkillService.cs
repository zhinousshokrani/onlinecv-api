using System.Collections.Generic;
using System.Linq;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.DataAccessLayer;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.BusinessLayer
{
    public class SkillService : ISkillService
    {
        IGenericRepository<Skill> _repository;
        public SkillService(IGenericRepository<Skill> repository)
        {
            _repository = repository;
        }

        public List<SkillWrapper> GetByCVId(int cvId)
        {
            return (from e in _repository.GetAll()
                    where e.CVId == cvId
                    select new SkillWrapper
                    {
                        skillName = e.skillName,
                        year = e.year,
                        institute = e.Institute
                    }).ToList();
        }

        public void SaveSkillHistory(SkillHistory skillHistory)
        {
            foreach (Skill s in skillHistory.skillList)
                s.CVId = skillHistory.cvId;

            //check if this cv id has previously skill history
            var previousSkillList = _repository.GetAll().Where(x => x.CVId == skillHistory.cvId).ToArray();
            int count = _repository.GetAll().Where(x => x.CVId == skillHistory.cvId).Count();
            if (count > 0)
                _repository.DeleteRange(previousSkillList);

            _repository.CreateRange(skillHistory.skillList);
        }
    }
}