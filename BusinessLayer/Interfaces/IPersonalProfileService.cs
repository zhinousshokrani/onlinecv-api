using OnlineCVAPI.Models;

namespace OnlineCVAPI.BusinessLayer.Interfaces
{
    public interface IPersonalProfileService
    {
        PersonalProfile GetById(int profileId);
        int SavePersonalPrfile(PersonalProfile profile);
    }
}