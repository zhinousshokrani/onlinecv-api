using System.Collections.Generic;
using System.IO;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.BusinessLayer.Interfaces
{
    public interface IResumeService
    {
        CV GetById(int cvId);
        FileStream GetResume(int cvId);
    }
}