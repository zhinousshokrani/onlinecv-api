using System.Collections.Generic;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.BusinessLayer.Interfaces
{
    public interface IEducationService
    {
        List<EducationWrapper> GetByCVId(int cvId);
        void SaveEducationHistory(EducationHistory educationList);
    }
}