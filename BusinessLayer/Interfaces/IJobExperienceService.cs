using System.Collections.Generic;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.BusinessLayer.Interfaces
{
    public interface IJobExperienceService
    {
        List<ExperienceWrapper> GetByCVId(int cvId);
        void SaveWorkHistory(WorkHistory jobList);
    }
}