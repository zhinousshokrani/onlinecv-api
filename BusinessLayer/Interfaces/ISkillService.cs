using System.Collections.Generic;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.BusinessLayer.Interfaces
{
    public interface ISkillService
    {
        List<SkillWrapper> GetByCVId(int cvId);
        void SaveSkillHistory(SkillHistory skillList);
    }
}